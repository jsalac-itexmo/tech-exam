<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\EmailVerificationController;


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});


Route::as('api.users.')
->controller(UserController::class)
->group(function () {
    Route::get("users", "all")->name("list");
    Route::get("users/{id}", "get")->name("get");
    Route::post("users", "store")->name("store");
    Route::patch("users/{id}", "update")->name("update");
    Route::delete("users/{id}", "destroy")->name("destroy");
})->middleware('auth:api');

Route::as('api.auth.')
->controller(AuthController::class)
->group(function () {
    Route::post("login", "login")->middleware('isverified')->name("login");
});

 
Route::get('/email/verify/{id}/{hash}', [EmailVerificationController::class, 'verify'])->name('verification.verify');