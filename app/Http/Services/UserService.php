<?php

namespace App\Http\Services;

use App\Models\User;
use Illuminate\Support\Str;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Log;
use App\Http\Resources\UserResource;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;

class UserService 
{
    public $model;
    
    public function __construct(User $user)
    {
        $this->model = $user;
    }


    /**
     * @param email string
     * @param password string
     */
    public function authenticate(string $email, string $password)
    {
        if(Auth::attempt(['email' => $email, 'password' => $password])) {

            $token = Auth::user()->createToken('email')->accessToken;
            $user = $this->model->find(Auth::user()->id);
            $user->last_login = Carbon::now();
            $user->save();
            $data = $user;
            $data["token"] = $token;
            return $data;
        }
        return null;
    }


    /**
     * @param $request Request
     * @param $id int - record id
     */
    public function store($request, $id = null)
    {
        /**
         * in case id is not null it will find the record and pass it to the model
         */
        if ($id != null) {
            $this->model = $this->model->find($id);
        }
        /**
         *  get the fillable column of model to iterate
         */
         $columns = $this->model->getFillable();
         $fileColumns = $this->model->fileColumns;

         /**
          * iterate on the column to pass the value from the payload
          */
          foreach($columns as $column) {
            if ($fileColumns && in_array($column, $fileColumns)) {

                /**
                 * the column is a file handler
                 * we need to store the file and get the path and pass the value to field to save it
                 */
                if ($request->file($column) !== null) {
                    $fileName = "avatar-".Str::uuid(10);
                    $this->model[$column] = $this->storeFile($request, $column, $fileName);
                } 
            } else {
                $this->model[$column] = $request[$column]  ?? $this->model[$column];
            }
          }
          
          $this->model->save();
          $this->model->sendEmailVerificationNotification();
          return new UserResource($this->model);
    }

    /**
     * @param $request Request
     * @param $columnName string - name of the field
     * @param $fileName string - filename you want for the file
     */
    public function storeFile($request, $columnName, $fileName)
    {

        if ($request->hasFile($columnName)) {

            $file = $request->file($columnName);
            $name = $fileName.".".$request->file($columnName)->getClientOriginalExtension();

            $filePath = storage_path('app/public/users');
            if(!File::exists($filePath)) {
                File::makeDirectory($filePath, 0777, true, true);
            }

            $path = $request->file($columnName)->storeAs("public/users", $name);
            return str_replace("public","storage", $path);
        } 
        return null;
    }
}
