<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;

class EmailVerificationController extends Controller
{
    

    public function verify($userId, Request $request)
    {
        if (!$request->hasValidSignature()) {
            return $this->respondUnAuthorizedRequest(401);
        }

        $user = User::findOrFail($userId);
        if ($user && !$user->hasVerifiedEmail()) {
            $user->markEmailAsVerified();
            return response()->json(["message" => "Email Successfully verified"]);
        } 
        return response()->json(["message" => "Invalid User Id"], 203);
    }


    public function resend()
    {
        
    }
}
