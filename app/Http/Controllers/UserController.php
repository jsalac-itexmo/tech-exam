<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\UserRequest;
use App\Http\Services\UserService;
use Illuminate\Support\Facades\Log;

class UserController extends Controller
{

    public $user ;

    public function __construct(UserService $userService)
    {   
        $this->user =  $userService;
    }

    /**
     * @param $request Illuminate\Http\Request;
     * @return UserResource
     */
    public function store(UserRequest $request) 
    {
        return $response = $this->user->store($request);
    }
    

}
