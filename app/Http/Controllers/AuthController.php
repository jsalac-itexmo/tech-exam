<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Services\UserService;
use App\Http\Requests\LoginRequest;

class AuthController extends Controller
{
    public $user;

    public function __construct(UserService $user) 
    {
        $this->user = $user;
    }

    
    public function login(LoginRequest $request)
    {
        $response = $this->user->authenticate($request->email, $request->password);
        if ($response == null) {
            return response()->json(["message" => "Invalid credentials"], 401);
        }
        return response()->json($response);
    }


}
